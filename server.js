var http = require('http');
var https = require("https");
var express =require("express");
var bodyParser = require('body-parser');
var request = require("request");

var app = express();
var server = http.createServer(app);

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/', function (req, res) {
    res.sendFile( __dirname + "/public/index.html" );
});

app.post('/sendmail', function (req, res) {

    var params = {
        to : [],
        cc : [],
        bcc : [],
        subject : '',
        message : '',
    }

    try{
        // validate email
        var validateEmail = function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        // parse email from text
        var parseEmail = function(_emails){
            var _result = [],
                _mails = _emails.split(',');
            for(let i=0; i<_mails.length; i++){
                if( validateEmail(_mails[i]) ){
                    _result.push({ email: _mails[i] });
                }
            }
            return _result;
        };

        // extend object
        var extend = function(target) {
            var sources = [].slice.call(arguments, 1);
            sources.forEach(function (source) {
                for (var prop in source) {
                    target[prop] = source[prop];
                }
            });
            return target;
        }

        params = JSON.parse(Object.keys(req.body)[0]);

        // log => received
        console.log(params);

        var _personalizations = { subject: params.subject };
        var _to = typeof(params.to)!=='undefined' ? parseEmail(params.to.toString()) : [];
        var _cc = typeof(params.cc)!=='undefined' ? parseEmail(params.cc.toString()) : [];
        var _bcc = typeof(params.bcc)!=='undefined' ? parseEmail(params.bcc.toString()) : [];

        if( _to.length ){ _personalizations = extend({}, _personalizations, {to: _to}); }
        if( _cc.length ){ _personalizations = extend({}, _personalizations, {cc: _cc}); }
        if( _bcc.length ){ _personalizations = extend({}, _personalizations, {bcc: _bcc}); }

        // log => emails
        console.log(_personalizations);

        var email_object = {
            personalizations: [ _personalizations ],
            from: { email: 'sender@example.com', name: 'TJ Dawa' },
            subject: params.subject,
            content: [
                { type: 'text/plain', value: params.message }
            ]
        };

        var options = { method: 'POST',
            url: 'https://api.sendgrid.com/v3/mail/send',
            headers:  {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer <TOKEN>'
            },
            body: email_object,
            json: true
        };

        request(options, function (error, response, body) {

            if( error ){
                console.log(error);
            }

            // if success
            if( response.statusCode == 200 || response.statusCode == 202 ){

                res.end(JSON.stringify({
                    result : 'Success. Email has been sent.',
                    error: ''
                }));

            }
            else{
                res.end(JSON.stringify({
                    result : '',
                    error: body.errors[0].message
                }));
            }
            
        });
    }
    catch(Exception){
        res.end(JSON.stringify({
            result : '',
            error: Exception.message
        }));
    }
    
})



var server = app.listen(3006, function () {
    // var host = server.address().address;
    var port = server.address().port;
    console.log("Server listening at http://127.0.0.1:%s", port)
});