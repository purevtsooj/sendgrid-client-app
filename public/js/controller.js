app.controller('emailFormCtrl', ['$scope', '$http', 'emailService', function($scope, $http, emailService){

	$scope.loading = false;
	$scope.alert = {
		show: false,
		class: 'uk-alert-success',
		message: '-'
	};

	[].forEach.call(document.querySelectorAll('input[type="tags"]'), tagsInput);

	$scope.formSubmit = function(){
		var _params = {
			to: $scope.email_to,
			cc: $scope.email_cc,
			bcc: $scope.email_bcc,
			subject: $scope.subject,
			message: $scope.message
		};

		$scope.loading = true;

		emailService.sendEmail( _params ).then(function(_data){
			try{
				
				if( _data.status==200 ){
					if( _data.data.error.length ){
						$scope.alert.message = _data.data.error;
						$scope.alert.class = 'uk-alert-danger';
						$scope.alert.show = true;
					}
					else{
						$scope.alert.message = _data.data.result;
						$scope.alert.class = 'uk-alert-success';
						$scope.alert.show = true;
					}
				}

				$scope.loading = false;
			}
			catch(e){
				$scope.loading = false;
			}
		});
	}

	return $scope;
}]);