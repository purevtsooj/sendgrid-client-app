app.factory('emailService', ['$http', '$rootScope', function($http, $rootScope){

	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

	var _sendEmail = function( params ){
		return $http({
	                method: 'POST',
	                url: '/sendmail',
	                data: params
	            });
	}

	return {
		sendEmail: _sendEmail
	};
}]);